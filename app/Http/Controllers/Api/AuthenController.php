<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth; 
use App\Models\User; 
use Validator;

class AuthenController extends Controller
{
    /** 
     * login api 
     * 
     * @return \Illuminate\Http\Response 
     */ 
    public function login(Request $request) { 
        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) { 
            $user = Auth::user(); 
            $accessToken =  $user->createToken('token')->accessToken; 

            return response()->json([
                'access_token' => $accessToken
            ], 200); 
        } 
        else { 
            return response()->json([
                'error' => 'Unauthorised'
            ], 401); 
        } 
    }
}
