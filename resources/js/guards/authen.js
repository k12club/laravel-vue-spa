import AuthenService from '@/services/authen-service';

const authenService = new AuthenService();
const guard = (to, from, next) => {
  if(authenService.isAuthen()) {
    next(); // allow to enter route
  } else {
    next('/'); // go to login;
  }
};

export default guard