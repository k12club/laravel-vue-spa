import Vue from 'vue';
import VueRouter from 'vue-router';
import BootstrapVue from 'bootstrap-vue'
import axios from 'axios';
import VueAxios from 'vue-axios';
import routes from './app-routing';
import store from './store';

Vue.use(BootstrapVue);
Vue.use(VueRouter);
Vue.use(VueAxios, axios);

const app = new Vue({
  el: '#app',
  store,
  router: new VueRouter(routes),
});
