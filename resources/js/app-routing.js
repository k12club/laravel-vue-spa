import HomeComponent from './components/home/home.vue';
import LayoutComponent from './components/main/layout/layout.vue';
import DashboardComponent from './components/main/dashboard/dashboard.vue';
import ParentComponent from './components/main/parent/parent.vue';
import Child1Component from './components/main/parent/child1/child1.vue';
import Child2Component from './components/main/parent/child1/child2/child2.vue';
import FoodComponent from './components/main/food/food.vue';
import Error404Component from './components/error/404.vue';
import authen from './guards/authen';

export default {
  mode: 'history',
  linkActiveClass: 'open active',
  scrollBehavior: () => ({ y: 0 }),
  routes: [
    {
      path: '/',
      name: 'Home',
      component: HomeComponent,
    },
    {
      path: '/main',
      component: LayoutComponent,
      beforeEnter : authen,
      children: [
        {
          path: 'dashboard',
          name: 'Dashboard',
          component: DashboardComponent,
        },
        {
          path: 'parent',
          meta: { label: 'Parent' },
          component: { render(c) { return c('router-view') } },
          children: [
            {
              path: '',
              component: ParentComponent,
            },
            {
              path: 'child1',
              meta: { label: 'Child1' },
              component: { render(c) { return c('router-view') } },
              children: [
                {
                  path: '',
                  component: Child1Component,
                },
                {
                  path: 'child2',
                  name: 'Child2',
                  component: Child2Component,
                },
              ]
            }
          ]
        },
        {
          path: 'food',
          name: 'Food',
          component: FoodComponent,
        }
      ]
    },
    {
      path: '*',
      name: 'Error404',
      component: Error404Component,
    }
  ]
}
