import Vue from 'vue';
import Vuex from "vuex";
import foodModule from "./food-module";

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    food: foodModule
  }
})