import FoodService from '@/services/food-service';
const foodService = new FoodService();

export default {
  namespaced: true,
  state: {
    foods: []
  },
  mutations: {
    FETCH_FOOD (state, payload) {
      state.foods = payload;
    },
    ADD_FOOD(state, payload) {
      state.foods.push(payload);
    },
    EDIT_FOOD(state, payload) {
      state.foods[payload.index] = payload;
    },
    REMOVE_FOOD(state, payload) {
      state.foods.splice(payload.index, 1);
    }
  },
  actions: {
    async fetchFood({ commit }) {
      const data = await foodService.fetchFood();
      commit('FETCH_FOOD', data);
    },
    async addFood({ commit }, payload) {
      const data = await foodService.addFood(payload);
      commit('ADD_FOOD', data);
    },
    async editFood({ commit }, payload) {
      const data = await foodService.editFood(payload);
      commit('EDIT_FOOD', data);
    },
    async removeFood({ commit }, payload) {
      const data = await foodService.removeFood(payload);
      commit('REMOVE_FOOD', data);
    }
  },
  getters: {
    foods: state => state.foods
  }
}
