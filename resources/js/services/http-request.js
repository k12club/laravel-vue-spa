import axios from 'axios';

class HttpRequest {
  constructor(url='/') {
    this.axiosInstance = axios.create({
      baseURL: url,
      timeout: 10000
    });

    this.axiosInstance.interceptors.request.use(
      (config) => {
        const token = localStorage.getItem('access_token');
        if (token) {
          config.headers.common['Authorization'] = `Bearer ${token}`;
        }
        return config;
      },
      (error) => Promise.reject(error.response));

    // Add a response interceptor
    this.axiosInstance.interceptors.response.use(
      (response) => {
        if (response.status === 200 || response.status === 201) {
          return Promise.resolve(response);
        } else {
          return Promise.reject(response);
        }
      },
      (error) => Promise.reject(error.response));
  }

  setHeader(header) {
    this.axiosInstance.defaults.headers.common = header;
    this.axiosInstance.defaults.headers.common['X-Requested-With'] = 'XMLHttpRequest';
    this.axiosInstance.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded';
    this.axiosInstance.defaults.headers.post['X-CSRF-TOKEN'] = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
  }

  get(methodName, data) {
    return this.axiosInstance.get(methodName, { 
      params: data 
    });
  }

  post(methodName, data) {
    return this.axiosInstance.post(methodName, data);
  }

  put(methodName, data) {
    return this.axiosInstance.put(methodName, data);
  }

  delete(methodName, param, data) {
    return this.axiosInstance.delete(methodName, {
      params: param,
      data: data
    });
  }

  request(type, url, data) {
    let promise = null
    switch (type) {
      case 'GET': promise = axios.get(url, { params: data }); break
      case 'POST': promise = axios.post(url, data); break
      case 'PUT': promise = axios.put(url, data); break
      case 'DELETE': promise = axios.delete(url, data); break
      default : promise = axios.get(url, { params: data }); break
    };
    return promise;
  }
}

export default HttpRequest