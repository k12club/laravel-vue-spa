import HttpRequest from './http-request'

class AuthenService extends HttpRequest {
  async login(payload) {
    try {
      const {data} = await this.post('api/authen/login', payload);
      localStorage.setItem('access_token', data.access_token);
      return Promise.resolve(true);
    } catch(e) {
      return Promise.reject(false)
    }
  }

  logout() {
    localStorage.removeItem('access_token');
    return Promise.resolve(true);
  }

  isAuthen() {
    return localStorage.getItem('access_token') != null;
  }
}

export default AuthenService