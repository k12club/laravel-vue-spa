import HttpRequest from './http-request'

class FoodService extends HttpRequest {
  async fetchFood() {
    const {data} = await this.get('api/food');
    return data;
  }

  async addFood(payload) {
    const {data} = await this.post('api/food', payload);
    return data;
  }

  async editFood(payload) {
    const {data} = await this.put(`api/food/${payload.id}`, payload);
    return data;
  }
  
  async removeFood(payload) {
    const {data} = await this.delete(`api/food/${payload.id}`);
    return data;
  }
}

export default FoodService