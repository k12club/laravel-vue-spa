<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}" />
        
        <title>Laravel Vue.JS</title>

        @if (App::environment('local'))
            <link rel="stylesheet" href="{{ asset('/css/app.css') }}">
        @else
            <link rel="stylesheet" href="{{ mix('/css/app.css') }}">
        @endif
    </head>
    <body class="antialiased">
        <noscript>
            <strong>We're sorry but this app doesn't work properly without JavaScript enabled. Please enable it to continue.</strong>
        </noscript>
        <main id="app">
            <router-view></router-view>
        </main>
    
        @if (App::environment('local'))
            <script src="{{ asset('/js/app.js') }}"></script>
        @else
            <script src="{{ mix('/js/app.js') }}"></script>
        @endif
    </body>
</html>
